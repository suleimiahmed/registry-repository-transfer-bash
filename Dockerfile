FROM docker:latest
# `curl` is required to make http requests
# `jq` is required to parse and format JSON
RUN apk update &&       \
    apk add --no-cache curl jq bash &&  \
    rm -rf /var/cache/apk/*