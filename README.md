# registry-repository-transfer-bash

An unofficial pipeline to migrate Gitlab container repositories from one Gitlab project to another.

_NOTE: This isn't battle tested, use at your own discretion._

![Annoyed L Jackson](./annoyed_l_jackson.jpeg "Title Text")

## Why this exists

As of the time of this tool/pipeline's inception; users with container repositories in their GitLab projects are unable to move/rename those projects. Efforts are being made to tackle this for projects on Gitlab.com https://gitlab.com/groups/gitlab-org/-/epics/9459 and eventually self-managed installs. 

For the time being however the workaround in https://docs.gitlab.com/ee/user/packages/container_registry/troubleshoot_container_registry.html#unable-to-change-project-path-or-transfer-a-project has been provided. 

This project simply builds on the same principle of the [documented workaround](https://docs.gitlab.com/ee/user/packages/container_registry/troubleshoot_container_registry.html#unable-to-change-project-path-or-transfer-a-project) by automating (to some degree) the process. In short, this project allows you to; copy all your repositories (and their images) to a **clean** dummy Gitlab project of your choice, after which you can delete all images in your existing project and then rename/move the project. Once the project has been successfully renamed/moved you can run the pipeline again (this time targeting the path of the newly transferred project) to copy the repositories (and their images) back from the dummy project to the transferred project.

## Pre-requisites

1. A PAT token associated with the user running the pipeline that is able to use the Gitlab API
2. A dummy project to copy the images to temporarily and back from

## How it works

There are two jobs in the pipeline `generate` and `migrate`. The pipeline's `generate` job generates a script with the commands that will be run during the migration from a source project to a destination project. This generated script is stored as a job artifact and can be reviewed by a user before running the `migrate` job. The `migrate` job is a manually triggered job that picks up the script created in the `generate` job and runs it.

You may also choose to download the generated script and run it locally as opposed to through the pipeline, this is the recommended approach if the runners seem to timeout (due to the sheer number of images) when running the `migrate` job.

The generated migration script relies on three simple docker commands; docker `pull`, `tag` and `push` to copy images between Gitlab projects. 

The majority of the time will be spent pulling images, the push part of the migration should be relatively fast given as all the images that are to be pushed can be mounted directly from the old repository to the new one.

## How to use

To use, fork and run the pipeline against your source and destination projects by re-setting the variables in the gitlab-ci.yml:

| Variable              | Description | Default |
| :---------------- | :------: | :----: |
| GITLAB_TOKEN        |   A gitlab token with access to gitlab APIs   |
| GITLAB_INSTANCE        |   Gitlab instance host   | gitlab.com |
| SRC_PROJECT_SPACE           |   The source project path including the namespace  | suleimiahmed/foo |
| DST_PROJECT_SPACE    | The destination project path including the namespace | suleimiahmed/bar |


After running all the jobs of the pipeline It is imperative to verify that all your images have been copied before deleting or archiving your old images. 

Inspired by https://gist.github.com/Smasherr/972272b56eebae3b860d62ef05eea6eb?permalink_comment_id=3336386
